# EDX 高性能可扩展编辑器

#### 介绍
此仓库为EDX编辑器的官方二进制仓库。EDX核心并未开源。

仓库主要用于收集反馈，建议，跟踪BUG。接收第三方扩展脚本。

[EDX官方网站](https://www.ed-x.cc)
=====================================================

## EDX是一款高性能的可扩展编辑器

- 它体积小巧，绿色无污染，开箱即用。
- 能快速打开大型文件，轻松编辑有千万行内容的文本
- 支持C/C++，LUA，JavaScript，Java，D，HTML，XML，CSS，CMake，JSON等格式的高亮显示
- 内建C/C++调试器。支持Win32/64及Linux程序的调试
- 内建lua脚本，用户可自由扩展功能
- 内建对MSVC，MSYS2/Mingw，clang，Intel oneAPI，WSL，llvm-mingw等工具集的支持
- C/C++支持基于clangd的语法提示，补全，跳转等基本操作
- 支持SSH远程开发
- 支持ESP32-IDF的CMake项目集成开发调试

### CMake工程内使用Clangd进行代码补全

![代码补全](https://www.ed-x.cc/imgs/code-completion.png)

### WSL下调试代码

![WSL下调试代码](https://www.ed-x.cc/imgs/wsl-debugging.png)

### 无需建立工程，立即模式下直接编译调试单个源代码

![立即模式下编译](https://www.ed-x.cc/imgs/instant-build.png)
![立即模式下编译](https://www.ed-x.cc/imgs/instant-debug.png)

### 支持SSH远程编译，调试应用

![立即模式下编译](https://www.ed-x.cc/imgs/build-ssh-target.png)

### 支持ESP32-IDF的CMake项目集成开发调试

![立即模式下编译](https://www.ed-x.cc/imgs/esp32-idf-project.png)

### Win11右键菜单支持
方法一：
    利用安装包中包含的相关文件生成MSIX包，然后自签名并安装

方法二：
    打开 系统设置 > 开发人员模式 > 从任意源安装应用 选项
    
    运行 regsvr32 shell_ext.dll 手工注册文件管理器扩展

    运行 powershell -command add-appxpackage -register appxmanifest.xml 注册应用
